#! /usr/bin/env bash

if [[ "$#" != 1 ]] ; then
  echo "Provide file to run on"
  exit 1
fi

FILE=$1
FILECSV=$(echo $FILE | sed 's/dat/csv/')
FILEYAML=$(echo $FILE | sed 's/dat/yaml/')

if [[ ! -f $FILE ]] ; then
  echo "$FILE does not exist"
  exit 2
fi

awk '/Voltage\[V\]/,EOF { print $1","$2 }' $FILE | grep -v Voltage > $FILECSV

python csv_to_yaml.py $FILECSV

# add temperature in yaml file
awk '/Temperature/ { print "#"$0 }' $FILE >> $FILEYAML
awk '/Type/ { print "#"$0 ; exit }' $FILE >> $FILEYAML

exit 0
