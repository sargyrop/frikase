# FRIKASE

**FR**eiburg **I**T**K** Me**AS**urem**E**nts

## Convert dat file to yaml for plotting

```
./convert_dat_to_yaml.sh VPA37913-W00221_IV_002.dat
```

## Plot

```
python plots.py
```
