#!/usr/bin/env python
import sys, csv

if len(sys.argv) != 2:
  print("python csv_to_yaml.py path/to/file.csv")
  sys.exit(1)

# Open our data file in read-mode.
filename=sys.argv[1]

x=[]
y=[]

# Read x and y values
with open(filename, 'r') as csvfile:
  csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
  for row in csvreader:
    if len(row) == 0: continue
    x.append(row[0])
    y.append(row[1])

yaml_filename = filename.replace('csv', 'yaml')

# Now dump x and y values in yaml file
with open(yaml_filename, 'w') as yamlfile:
    yamlfile.write("dependent_variables:\n")
    yamlfile.write("- values:\n")
    for i in range(len(y)):
        yamlfile.write("  - value: " + y[i] + "\n")
    yamlfile.write("independent_variables:" + "\n")
    yamlfile.write("- values:" + "\n")
    for i in range(len(x)):
        yamlfile.write("  - value: " + x[i] + "\n")


