import yaml
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from math import log10,exp,sqrt
import os
import numpy as np

### Global plot settings
rcParams['hatch.linewidth'] = 0.2
#rcParams["mathtext.fontset"] = "cm"
MAJOR_TICK_LENGTH = 6
MINOR_TICK_LENGTH = 3
ALPHA=0.2

### Plot class
class Plot:
    def __init__(self, x, y, titleX, titleY, color, format, rangeX, rangeY, legend, name):
        self.x = x
        self.y = y
        self.titleX = titleX
        self.titleY = titleY
        self.color  = color
        self.format = format
        self.rangeX = rangeX
        self.rangeY = rangeY
        self.legend = legend
        self.name   = name

### Function to get stuff from yaml files
def getXYfromYAML(inputFile):
    f = open(inputFile)
    parsed = yaml.load(f, Loader=yaml.FullLoader)
    # Get X variable
    y = parsed.get('dependent_variables')[0].get('values')
    x = parsed.get('independent_variables')[0].get('values')
    x_list = [float(i.get('value')) for i in x]
    y_list = [float(i.get('value')) for i in y]
    return np.asarray(x_list), np.asarray(y_list)

### Function that does the actual plotting
def plot(Plots):
    loop=1
    for p in Plots:
        plt.plot(p.x, p.y, p.format, color=p.color, label=p.legend)
           
    # Plot cosmetics
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel(Plots[0].titleX, fontsize=15)
    plt.ylabel(Plots[0].titleY, fontsize=15)
    plt.xlim([Plots[0].rangeX[0], p.rangeX[1]])
    plt.ylim([Plots[0].rangeY[0], p.rangeY[1]])
    # Change how the tick marks are printed
    axes = plt.gca()
    axes.tick_params("both", which="both", direction="in")
    axes.tick_params("both", which="major", length=MAJOR_TICK_LENGTH)
    axes.tick_params("both", which="minor", length=MINOR_TICK_LENGTH)
    axes.tick_params("x", which="both", top=True)
    axes.tick_params("y", which="both", right=True)
    plt.legend(loc='upper right', handlelength=2.5, prop={'size': 8.5})
    plt.grid(visible=True)
    plt.savefig(f'{Plots[0].name}', bbox_inches="tight")
    plt.close()

### Function to get sensor area
def sensorArea(type):
    AreaDict={
        'Unknown':(97.621-0.450)*(97.950-0.550),
        #
        'ATLAS12': 95.7*95.64,
        'ATLAS17LS': (97.621-0.450)*(97.950-0.550),
        #
        'ATLAS18R0':89.9031,
        'ATLAS18R1':89.0575,
        'ATLAS18R2':74.1855,
        'ATLAS18R3':80.1679,
        'ATLAS18R4':87.4507,
        'ATLAS18R5':91.1268,
        #
        'ATLAS18SS':93.6269,
        'ATLAS18LS':93.6269,
        #
        'ATLASDUMMY18':(97.621-0.450)*(97.950-0.550),
    }
    return AreaDict[type]


### Function that does the IV analysis
def getIV(file):
    print(f"=== Analyse: {file} ===")
    
    x,y = getXYfromYAML(file)
    x *= -1
    y *= -1
    with open(file, "r") as f:
        for line in f:
            if "Temperature" in line:
                T=float(line.split(':')[-1].strip())
            elif "Type" in line:
                sensorType=line.split(':')[-1].strip()
    
    # temperature correction
    corr_factor = (20./T)**2 * exp(- (1.2/(2.*8.617E-5)) * (1./(293.15) - 1./(T+273.15)) )
    print(f"\t T(20) correction factor : {corr_factor}")
    y_c = y * corr_factor
    
    # RMS calculation
    StabilityV = 700
    arr_V = np.array(x)
    arr_I = np.array(y)
    IStability = np.abs(arr_I[np.where(arr_V == StabilityV)])
    if np.size(IStability) == 4:
        IVariation = abs(np.std(IStability)/np.mean(IStability))
        print(f"\t RMS_Stability : {IVariation}")
    
    # Normalised value of current at 500 V normalised to sensor area
    I_500V = np.abs(np.array(y_c)[np.where(arr_V == 500)])[0] / sensorArea(sensorType)
    print(f"\t I_500V : {I_500V}")
    
    return x,y,y_c
    
########################################################################

# Here we define what we want to plot

xmin = 0
xmax = 700
ymin = 0
ymax = 200

x,y,y_c = getIV("VPA37913-W00221_IV_001_FR.yaml")
FR = Plot(x,y,r"$V$ [V]",r"$I$ [nA]", "blue", "-",  [xmin,xmax],[ymin,ymax], "FR", "comp")
FR_c = Plot(x,y_c,r"$V$ [V]",r"$I$ [nA]", "blue", "--",  [xmin,xmax],[ymin,ymax], "FR (T=20)", "comp")

x,y,y_c = getIV("VPA37913-W00221_IV_002.yaml")
FR2 = Plot(x,y,r"$V$ [V]",r"$I$ [nA]", "blue", "-",  [xmin,xmax],[ymin,ymax], "FR", "comp")
FR2_c = Plot(x,y_c,r"$V$ [V]",r"$I$ [nA]", "blue", "--",  [xmin,xmax],[ymin,ymax], "FR (T=20)", "comp")

#x,y,y_c = getIV("VPA37913-W00218_IV_008.yaml")
#FR2 = Plot(x,y,r"$V$ [V]",r"$I$ [nA]", "orange", "-",  [xmin,xmax],[ymin,ymax], "FR2", "comp")
#FR2_c = Plot(x,y_c,r"$V$ [V]",r"$I$ [nA]", "orange", "--",  [xmin,xmax],[ymin,ymax], "FR2 (T=20)", "comp")
#
#x,y,y_c = getIV("VPA37913-W00218_IV_009.yaml")
#FR3 = Plot(x,y,r"$V$ [V]",r"$I$ [nA]", "purple", "-",  [xmin,xmax],[ymin,ymax], "FR3", "comp")
#FR3_c = Plot(x,y_c,r"$V$ [V]",r"$I$ [nA]", "purple", "--",  [xmin,xmax],[ymin,ymax], "FR3 (T=20)", "comp")

x,y,y_c = getIV("VPA37913-W00221_IV_001.yaml")
SFU = Plot(x,y,r"$V$ [v]",r"$I$ [nA]", "red", "-",  [xmin,xmax],[ymin,ymax], "SFU", "comp")
SFU_c = Plot(x,y_c,r"$V$ [V]",r"$I$ [nA]", "red", "--",  [xmin,xmax],[ymin,ymax], "SFU (T=20)", "comp")


#plot([FR,SFU,FR_c,SFU_c,FR2,FR2_c,FR3,FR3_c])
plot([FR2, FR2_c, SFU, SFU_c])
